FROM rust:slim

RUN apt update -qq && apt install -qqy libssl-dev pkg-config
RUN apt-get install -y sshpass openssh-client;

ONBUILD RUN echo "\n\n============================================================\nbuild using $(rustc --version)\n============================================================\n\n"
#e n d